let siteMetadata = {
  title: `OSL`,
  capitalizeTitleOnHome: true,
  logo: `/images/logo_light.png`,
  who: `/images/who.jpg`,
  card1: `/images/mentor.svg`,
  card2: `/images/talk.svg`,
  card3: `/images/code.svg`,
  join1: `/images/develop.svg`,
  join2: `/images/mentoring.svg`,
  join3: `/images/opportunities.svg`,
  join4: `/images/career.svg`,
  icon: `/images/favicon.png`,
  titleImage: `/images/wall.jpg`,
  introTag: `PHOTOGRAPHER | VIDEOGRAPHER`,
  description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet accumsan arcu. Proin ac consequat arcu.`,
  author: `@_akzhy`,
  blogItemsPerPage: 10,
  portfolioItemsPerPage: 10,
  darkmode: true,
  switchTheme: true,
  navLinks: [
    {
      name: "HOME",
      url: "/",
    },
    {
      name: "MEMBERS",
      url: "/members",
    },
    {
      name: "BLOGS",
      url: "/blogs",
    },
    {
      name: "PROJECTS",
      url: "/projects",
    },
    {
      name: "CONTACT",
      url: "/contact",
    },
  ],
  footerLinks: [
    {
      name: "PRIVACY POLICY",
      url: "/privacy-policy",
    },
    {
      name: "GitHub",
      url: "https://github.com/akzhy/gatsby-starter-elemental",
    },
  ],
  social: [
    {
      name: "Facebook",
      icon: "/images/Facebook.svg",
      url: "#",
    },
    {
      name: "Twitter",
      icon: "/images/Twitter.svg",
      url: "#",
    },
    {
      name: "Instagram",
      icon: "/images/Instagram.svg",
      url: "#",
    },
    {
      name: "Youtube",
      icon: "/images/Youtube.svg",
      url: "#",
    },
  ],
  contact: {
    /* Leave the below value completely empty (no space either) if you don't want a contact form. */
    api_url: "./test.json",
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet accumsan arcu. Proin ac consequat arcu.`,
    mail: "hi@akzhy.com",
    phone: "000-000-0000",
    address: "1234 \nLocation \nLocation",
  },
}

module.exports = {
  siteMetadata: siteMetadata,
  plugins: [
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
        stripMetadata: true,
        defaultQuality: 75,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          "gatsby-remark-copy-linked-files",
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1280,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `contents`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-plugin-less`,
      options: {
        strictMath: true,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages/`,
      },
    },
  ],
}
