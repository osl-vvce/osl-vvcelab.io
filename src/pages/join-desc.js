import React from "react"
import Layout from "../components/layout"
import "../styles/join-desc.less"

const Join = () => {
  return (
    <Layout>
      <div className="join-container">
        <div className="row1">
          <h2 className="heading">Advance technically to new Heights</h2>
          <p className="para">
            Web, Android, Data Science, Desktop Application Development,
            Networks, IoT- learn and prosper under great mentorship and
            community support, and use your skills to help making real time
            software in Open Source. Make an impact!
          </p>
        </div>

        <div className="row1">
          <h2 className="heading">Receive care & support through mentorship</h2>
          <p className="para">
            Receive care and support from our rich mentorship environment. With
            the help of experienced folks - solve your issues, be it technical
            or non technical, set goals and achieve them all.
          </p>
        </div>

        <div className="row1">
          <h2 className="heading">Become competent to face the real world</h2>
          <p className="para">
            Sharpen your skills, get exposed to collaborative product making, be
            a part and lead developer teams and contribute to real time Open
            Source software - all the while developing a world class working
            style. Grow!
          </p>
        </div>

        <div className="row1">
          <h2 className="heading">Surf in an ocean of opportunities</h2>
          <p className="para">
            Internships, Student Exchange Programs, Summer Schools, Conferences,
            Talks, Hackathons - be it GSoC, SoK, RGSoC, Outreachy, FOSDEM etc -
            get exposed to the best of opportunities out there and prove your
            metal in the real world. Carpe diem!
          </p>
        </div>
      </div>
    </Layout>
  )
}

export default Join
