import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import "../styles/blog.less"
const BlogPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <div className="home-container">
        <div className="heading">
          <h1>BLOG</h1>
        </div>

        <div className="content">
          {data.allMarkdownRemark.edges.map(edge => {
            return (
              <Link to={`/blog/${edge.node.fields.slug}`}>
                <h2>{edge.node.frontmatter.title}</h2>
                <p>{edge.node.frontmatter.date}</p>
              </Link>
            )
          })}
        </div>
      </div>
    </Layout>
  )
}

export default BlogPage
