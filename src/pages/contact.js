import React from 'react'
import Layout from '../components/layout'

const ContactPage = () => {
    return(
        <Layout>
            <div>
                This is contact page.
            </div>
        </Layout>
    )
}

export default ContactPage