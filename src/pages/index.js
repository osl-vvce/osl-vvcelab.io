import React from "react"
import Layout from "../components/layout"
import { Link } from "gatsby"
import SocialLinks from "../components/sociallinks"
import { useStaticQuery, graphql } from "gatsby"
import "../styles/index.less"
import "../styles/wall.less"

const IndexPage = () => {
  const query = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          who
          card1
          card2
          card3
          join1
          join2
          join3
          join4
        }
      }
    }
  `)
  return (
    <Layout>
      <div>
        <div className="wall" id="row">
          <div className="social-buttons" id="column1">
            <SocialLinks />
          </div>
          <div className="heading" id="column2">
            <h1>Open Source Lab</h1>
            <h3>We do Awesome things</h3>
          </div>
        </div>
        <div className="home-container">
          <div className="description">
            <p className="description">
              OSL, a student-run community with over 20+ members from
              Vidyavardhaka College of Engineering, Mysuru, over the last 2
              years, has helped 100+ passionate students to transform into
              successful & innovative engineers who today work across the world
              in Fortune 500 Companies loving what they do.
            </p>
          </div>

          <div className="row">
            <div className="column4">
              <img src={query.site.siteMetadata.who} alt="nothing" id="img1" />
            </div>
            <div className="column3">
              <h1 className="heading">Who We Are</h1>
              <p>
                Open Source Lab (also known as OSL ) is a community of
                passionate college students, who have come together for the
                cause of promoting and contributing to Free and Open Source
                Software, whilst achieving excellence in the field of technology
                in their pursuit of knowledge. <br />
                <br />
                The club envisions to make its members competent to become the
                leaders of the future with excellence in technical, creative,
                and intrapersonal skills, through making them learn how to
                learn, by simulating a workspace like atmosphere inside the lab,
                and making them go through all challenges, and setbacks they may
                face in the real world. <br /> <br />
                Founded in 2017, the club over the years has been renowned as
                one of the leading student clubs in India, with about 500 active
                members. It has so far produced 1 GSoCer, and every year more
                than 3 teams make it to the Grand Finale of SIH. The club is run
                by the students themselves, with the help of research scholars,
                alumni, and faculties.
              </p>
            </div>
          </div>

          <div className="row">
            <div className="column3">
              <h1 className="heading">What We Do</h1>
              <p>
                The club operates by the principle of mentorship and family,
                where the senior members of the club individually mentor the
                juniors, and the club as such acting as a family where the one
                helps the other out. <br />
                <br />
                The members contribute to Open Source projects of various
                popular open source organizations, work on developing their own
                projects including the club projects aimed at solving real world
                problems, participate in GSoC, developer meets, hackathons etc.
                The members also conduct workshops and technical fests to
                encourage others to work on open source technologies. <br />
                <br />
                The club also got specialised teams internally such as one
                focused on Android, Blockchain, IoT, Data Science, Machine
                Learning, Problem-solving, UI/UX, and Web Development. <br />
                <br />
                The club believes in the holistic development of its members,
                and the members besides seeking excellence in technical skills,
                also engage in talks, debates, sports, cultural activities, yoga
                sessions etc. <br />
                <br />
              </p>
            </div>
            <div className="column4">
              <img src={query.site.siteMetadata.who} alt="nothing" id="img1" />
            </div>
          </div>

          <div className="cards-01">
            <div className="card-1">
              <img src={query.site.siteMetadata.card1} alt="Avatar" />
              <div className="container-card">
                <h5>
                  <b>Mentor & train students to take on the world</b>
                </h5>
              </div>
            </div>

            <div className="card-1">
              <img src={query.site.siteMetadata.card2} alt="Avatar" />
              <div className="container-card">
                <h5>
                  <b>Promote Open Source and Computer Science</b>
                </h5>
              </div>
            </div>

            <div className="card-1">
              <img src={query.site.siteMetadata.card3} alt="Avatar" />
              <div className="container-card">
                <h5>
                  <b>Contribute to Open Source Projects & Communities</b>
                </h5>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="column4">
              <img src={query.site.siteMetadata.who} alt="nothing" id="img1" />
            </div>
            <div className="column3">
              <h1 className="heading">What we have achieved?</h1>
              <p>
                The club has become one of the forerunners in promoting, and
                contributing to open source in the country, introducing over a
                hundred enthusiasts every year to the world of Open source
                through the various activities it conducts. <br />
                <br /> The club has produced 12 teams who have made it through
                SIH Grand Finale over the last 3 years, with about 3 teams
                getting selected every year. <br />
                <br /> The members have been actively taking part in
                international conferences, meetups, hackathons, as well as
                delivering talks and presenting projects at various events.
                Majority of the club alumni have been placed in core computer
                science companies. The active members also get selected for
                summer schools at global universities, and paid internships at
                top companies. Some of our alumni are currently Phd. research
                scholars at various foreign universities.
              </p>
            </div>
          </div>

          <div className="cards-02">
            <div className="card-2">
              <img src={query.site.siteMetadata.card1} alt="Avatar" />
              <div className="container-card">
                <h5>
                  <b>100+</b>
                </h5>
                <p>Students trained</p>
              </div>
            </div>

            <div className="card-2">
              <img src={query.site.siteMetadata.card2} alt="Avatar" />
              <div className="container-card2">
                <h5>
                  <b>1+</b>
                </h5>
                <p>GSoC Students</p>
              </div>
            </div>

            <div className="card-2">
              <img src={query.site.siteMetadata.card3} alt="Avatar" />
              <div className="container-card2">
                <h5>
                  <b>10+</b>
                </h5>
                <p>Alumni in Core CS Jobs</p>
              </div>
            </div>

            <div className="card-2">
              <img src={query.site.siteMetadata.card3} alt="Avatar" />
              <div className="container-card2">
                <h5>
                  <b>5+</b>
                </h5>
                <p>Working in Startups</p>
              </div>
            </div>
          </div>

          <div className="heading3">
            <Link to="/join-desc">
              <h1>Why Join Us?</h1>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage
